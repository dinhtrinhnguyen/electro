<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return view('pages.index');
});
Route::get('home', function () {
    return view('pages.index');
});
Route::get('index', 'HomeController@index');
Route::get('product', 'HomeController@product');
Route::get('store', 'HomeController@store');
Route::get('checkout', 'HomeController@checkout');
Route::get('about/{param}', 'HomeController@about');
Route::get('abc/123', array( 'as' => '123', function() {
    $theURL = URL::route('123');
    return "link trang test URL: $theURL";
}));
Route::get('login', 'UserController@login')->name('login');
Route::post('login', 'UserController@postLogin')->name('postlogin');
Route::get('register', 'UserController@register')->name('register');

Route::group(['prefix' => 'admin'], function() {
    Route::get('/', function() {
        return view('admin.dashboard');
    });
});

// Route::get('admincp/login', ['as' => 'getLogin', 'uses' => 'Admin\AdminLoginController@getLogin']);
// Route::post('admincp/login', ['as' => 'postLogin', 'uses' => 'Admin\AdminLoginController@postLogin']);
// Route::get('admincp/logout', ['as' => 'getLogout', 'uses' => 'Admin\AdminLoginController@getLogout']);

// Route::group(['middleware' => 'checkAdminLogin', 'prefix' => 'admincp', 'namespace' => 'Admin'], function() {
// 	Route::get('/', function() {
// 		return view('admin.home');
// 	});
// });