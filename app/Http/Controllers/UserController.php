<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login()
    {
    	return view('layouts.login');
    }

    public function postLogin(Request $req)
    {
    	// this->user($req, [],[]) 
    	return redirect('admin');
    }

    public function register()
    {
    	return view('layouts.register');
    }
}
