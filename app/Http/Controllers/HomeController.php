<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	return view('index');
    }
    
    public function product()
    {
    	return view('pages.product');
    }

    public function store()
    {
    	return view('pages.store');
    }

    public function checkout()
    {
    	return view('pages.checkout');
    }

    public function about($t)
    {
    	return "Hello $t";
    }
}
