<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
        	'name' => "admin",
        	'email' => 'admin@email.com',
        	'password' => bcrypt('12345678'),
        	'level' => 1,
        	'status' => 1,
        ]);
        // DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
    }
}
